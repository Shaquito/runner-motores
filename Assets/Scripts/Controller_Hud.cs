﻿using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class Controller_Hud : MonoBehaviour
{
    public static bool gameOver = false;
    public Text distanceText;
    public Text gameOverText;
    public Text textoVidas;
    private float distance = 0;

    void Start()
    {
        gameOver = false;
        distance = 0;
        distanceText.text = distance.ToString();
        gameOverText.gameObject.SetActive(false);
    }

    void Update()
    {
        textoVidas.text = Controller_Player.vida.ToString() + " Vidas " + Controller_Player.municion.ToString() + " Balas";//Muestra la cantidad de vidas en pantalla

        if (gameOver)//Cuando pierde el tiempo se detiene, se hace una conversion de la distancia float hacia int para redondear
        {
            Time.timeScale = 0;
            distance = (int)distance;
            gameOverText.text = "Game Over \n Total Distance: " + distance.ToString();
            gameOverText.gameObject.SetActive(true);
        }
        else//Sino muestra la distancia actual recorrida
        {
            distance += Time.deltaTime;
            distanceText.text = distance.ToString("F0");
        }
    }
}
