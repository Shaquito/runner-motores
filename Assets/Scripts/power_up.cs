using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class power_up : MonoBehaviour
{
   
    // Start is called before the first frame update
    void Start()
    {
     
    }

    // Update is called once per frame
    void Update()
    {
       
    }
    // Cuando el powerUp collisiona contra algo se destruye, le suma una bala y le suma una vida al player si este tiene menos de 3
    void OnTriggerEnter(Collider other)
    {
        Controller_Player.municion += 1;
        Destroy(gameObject);
        if(Controller_Player.vida < 3)
        {
            Controller_Player.vida += 1;
            
        }
    }
}

