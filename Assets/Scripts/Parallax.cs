﻿using UnityEngine;

public class Parallax : MonoBehaviour
{
    public GameObject cam;
    private float length, startPos;
    public float parallaxEffect;

    void Start()
    {//Guarda la posicion inicial y el largo del sprite
        startPos = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    void Update()
    {
        if (Controller_Hud.gameOver == true)//Si pierde el parallax deja de moverse
        {
            parallaxEffect = 0;
        }

        transform.position = new Vector3(transform.position.x - parallaxEffect, transform.position.y, transform.position.z);//Se mueve x frame hacia la izquierda
        if (transform.localPosition.x < -20)//Cuando llega a -20 lo vuelve a posicionar en 20
        {
            transform.localPosition = new Vector3(20, transform.localPosition.y, transform.localPosition.z);
        }
    }
}
