﻿using UnityEngine;

public class Controller_Enemy : MonoBehaviour
{
    public static float enemyVelocity;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {//Cada frame le agrega fuerza al eje X del objeto hacia la izquierda
        rb.AddForce(new Vector3(-enemyVelocity, 0, 0), ForceMode.Force);
        OutOfBounds();
    }

    public void OutOfBounds()
    {//Cuando el objeto llego a x menor a menos 40 lo elimina
        if (this.transform.position.x <= -40)
        {
            Destroy(this.gameObject);
        }
    }
}
