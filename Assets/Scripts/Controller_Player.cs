﻿using System.Collections.Specialized;
using System.Security.Cryptography;
using UnityEngine;

public class Controller_Player : MonoBehaviour
{
    private Rigidbody rb;
    public float jumpForce = 10;
    private float initialSize;
    private int i = 0;
    private bool floored;
    public static int vida = 3;
    public GameObject textoDistancia;
    public static int municion = 0;
    public GameObject bala;
    public Transform cañon;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        initialSize = rb.transform.localScale.y;
    }

    void Update()
    {
        //Comprueba los inputs y si el jugador perdio
        GetInput();
        if (vida == 0)
        {
            //Si el jugador pierde se destruye y elimina el texto que iba mostrando la distancia actual
            Destroy(this.gameObject);
            Controller_Hud.gameOver = true;
            Destroy(textoDistancia.gameObject);
        }
    }

    private void GetInput()
    {
        Jump();
        Duck();
        Disparar();
    }

    private void Disparar()
    {
        if (Input.GetKeyDown(KeyCode.Space) && municion > 0)
        {
            Instantiate(bala, cañon.position, cañon.rotation);
            municion--;
        }
    }

    private void Jump()// Si esta en el piso y presiona la tecla W aplica una fuerza en el eje Y
    {
        if (floored)
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
            }
        }
    }

    private void Duck()//Si el jugador esta en el piso achica su escala en Y a la mitad
    {
        if (floored)
        {
            if (Input.GetKey(KeyCode.S))
            {
                if (i == 0)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, rb.transform.localScale.y / 2, rb.transform.localScale.z);
                    i++;
                }
            }
            else //Si esta en el suelo pero su escala es diferente a cuando comenzo, le devuele la escala normal
            {
                if (rb.transform.localScale.y != initialSize)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, initialSize, rb.transform.localScale.z);
                    i = 0;
                }
            }
        }
        else//Si no esta en el suelo aplica la fuerza de salto hacia abajo x 2
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                rb.AddForce(new Vector3(0, -jumpForce * 2, 0), ForceMode.Impulse);
            }
        }
    }

    public void OnCollisionEnter(Collision collision)//Cuando colisione con algo va a comprarar contra que
    {
        if (collision.gameObject.CompareTag("Enemy"))//Si es contra un enemigo le restara una vida al player ademas de eliminar al enemigo
        {
            vida -= 1;
            Destroy(collision.gameObject);
        }

        if (collision.gameObject.CompareTag("Floor"))//Si toca el suelo la variable floored sera true
        {
            floored = true;
        }
    }

    private void OnCollisionExit(Collision collision)//Cuando deja de tocar el suelo floored sera false
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = false;
        }
    }
}
