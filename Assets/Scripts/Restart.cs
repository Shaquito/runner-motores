﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{
    void Update()
    {
        GetInput();
    }

    private void GetInput()
    {
        if (Input.GetKeyDown(KeyCode.R))//Si presiona la R devuelve el tiempo al normal, le setea las vidas en 3 y vuelve a recargar la escena
        {
            Time.timeScale = 1;
            Controller_Player.vida = 3;
            SceneManager.LoadScene(0);
        }
    }
}
 